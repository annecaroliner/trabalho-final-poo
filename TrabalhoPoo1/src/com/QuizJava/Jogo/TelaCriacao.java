package com.QuizJava.Jogo;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class TelaCriacao extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton btn;
	private JLabel lb;
	private JTextField txtField; 
	
	TelaCriacao(){
		this.setTitle("Quiz do Java");
		this.setSize(500,250);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(null);
		/* this.add(initImagen());  imagem p/ tela de usuario */
		this.add(initLabel());
		this.add(initTextField());
		this.add(initButton());
		this.setVisible(true);
	}
	
	/*private Component initImagen() {
		ImageIcon image = new ImageIcon(getClass().getResource("download.jpg")); // imagem para a tela de usuario
		JLabel i = new JLabel(image);
		i.setBounds(0, 0, 400, 400);
		return i; 
	} */
	
	private JLabel initLabel() {
		lb = new JLabel("Diga seu nome:");
		lb.setBounds(115,10,275,50);
		lb.setFont(new Font("Arial",Font.BOLD,17));
		lb.setHorizontalAlignment(JTextField.CENTER);
		return lb;
	}
	
	private JTextField initTextField() {
		txtField = new JTextField();
		txtField.setFont(new Font("Arial",Font.BOLD,15));
		txtField.setHorizontalAlignment(JTextField.CENTER);
		txtField.setBounds(150,80, 200, 35);
		return txtField;
	}
	
	private JButton initButton() {
		btn = new JButton();
		btn.setFont(new Font("Arial",Font.BOLD,15));
		btn.setText("Jogar");
		btn.setBounds(200, 140, 100, 30);
		btn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(txtField.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Voc� deve colocar o seu nome!");
				}else {
					Jogador a = new Jogador();
					a.setName(txtField.getText());
					System.out.println("Jogador Criado");
					new Game(a).Jogar();
					dispose();
				}
			}
			
		});
		
		return btn;
	}
	
}
